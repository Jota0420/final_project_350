'''
Name: Jose Daniel Orrego
Section leader:  Stephen Kim
Date: 11/30/2021
Ista 350 Final Project
Description: Final Project
'''

import pandas as pd, matplotlib.pyplot as plt, statsmodels.api as sm
import numpy as np
import seaborn as sns
import opendatasets as od
import os


'''
Scrapes the Data from kaggle about car sales
'''

def get_datasets():
    dataset = "https://www.kaggle.com/gagandeep16/car-sales/code"
    od.download(dataset)
    data_dir = './car-sales'
    car_sales_df = pd.read_csv('car-sales/Car_sales.csv')

    return car_sales_df

car_sales_df = get_datasets()


'''
Creates a group of the data set sorting by highest price
'''
price_data = car_sales_df[:55].groupby("Manufacturer", as_index=False)['Price_in_thousands'].sum().sort_values(by= 'Price_in_thousands', ascending = False) 

'''
Creates a barplot
'''
ax = sns.barplot(x='Manufacturer', y='Price_in_thousands', data=price_data, palette="Spectral")
plt.title('Average Price by Manufacturer (Top 10)', fontsize=30, pad = 20)
plt.xlabel('')
plt.ylabel('Price in thousands', fontsize=22, labelpad = 20)
plt.yticks(fontsize = 22)
plt.xticks(fontsize=10, rotation = -45, position = (0,-0.01))

for i in ax.patches:
    ax.text(i.get_x()+i.get_width()/2, i.get_height()+1, int(i.get_height()),  ha='center', va='bottom', fontsize = 10)

plt.show()

'''
Creates a pie chart
'''
data = car_sales_df[:60].groupby("Manufacturer")['Sales_in_thousands'].sum().sort_values(ascending = False)

pie, ax = plt.subplots(figsize=[10,6])
labels = data.keys()
plt.pie(x=data, autopct="%.1f%%", explode = (0,0,0,0,0.1,0.2,0.3,0.4,0.5,0.6), labels=labels, pctdistance=0.5)
plt.title("Percentage of Sales (Top 10)", fontsize=14);