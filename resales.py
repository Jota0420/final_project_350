'''
Name: Jose Daniel Orrego
Section leader:  Stephen Kim
Date: 11/30/2021
Ista 350 Final Project
Description: Final Project
'''

import pandas as pd, matplotlib.pyplot as plt, statsmodels.api as sm
import numpy as np
import seaborn as sns
import opendatasets as od
import os


'''
Scrapes the Data from kaggle about car sales
'''
def get_datasets():
    dataset = "https://www.kaggle.com/gagandeep16/car-sales/code"
    od.download(dataset)
    data_dir = './car-sales'
    car_sales_df = pd.read_csv('car-sales/Car_sales.csv')

    return car_sales_df

car_sales_df = get_datasets()

'''
Creates a group of the data set sorting it by highest resale value
'''

resale_data = car_sales_df[:55].groupby("Manufacturer", as_index=False)['__year_resale_value'].sum().sort_values(by= '__year_resale_value', ascending = False) 

'''
Creates the barplot
'''
ax = sns.barplot(x='__year_resale_value', y='Manufacturer', data=resale_data, palette="icefire")
plt.title('Average Resale Price by Manufacturer (Top 10)', fontsize=30, pad = 20)
plt.xlabel('')
plt.ylabel('Price in thousands', fontsize=22, labelpad = 20)
plt.yticks(fontsize = 15)
plt.xticks(fontsize=15, rotation = -45, position = (0,-0.01))

plt.show()