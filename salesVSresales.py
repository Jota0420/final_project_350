'''
Name: Jose Daniel Orrego
Section leader:  Stephen Kim
Date: 11/30/2021
Ista 350 Final Project
Description: Final Project
'''

import pandas as pd, matplotlib.pyplot as plt, statsmodels.api as sm
import numpy as np
import seaborn as sns
import opendatasets as od
import os


'''
Scrapes the Data from kaggle about car sales
'''
def get_datasets():
    dataset = "https://www.kaggle.com/gagandeep16/car-sales/code"
    od.download(dataset)
    data_dir = './car-sales'
    car_sales_df = pd.read_csv('car-sales/Car_sales.csv')

    return car_sales_df

car_sales_df = get_datasets()

'''
Creates a Scatterplot that checks correlation between resale price and total sales
'''

sns.lmplot(x="Sales_in_thousands",y="__year_resale_value",data=car_sales_df, line_kws={'color': 'purple'})
plt.title("Correlation between Resale Price and Total Sales", fontsize = 25, pad = 20)
plt.xlabel("Total Sales in Thousands", fontsize = 20)
plt.ylabel("Resale value in Thousands", fontsize = 20)
plt.yticks(fontsize = 19)
plt.xticks(fontsize = 19)

plt.show()